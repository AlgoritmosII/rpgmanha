package com.company;

//superclasse
public abstract class Personagem {
    //TODO: Alterar tipos para double
    protected String nome;
    protected int forca;
    protected int habilidadeMental;
    protected int poderDeAtaque;
    protected int esquiva;
    protected int resistencia;
    protected int vidaAtual;
    protected int vidaMaxima;

    public abstract void atacar(Personagem oponente);

    public abstract void andar();

    public void contraatacar(Personagem oponente){

    }
    public void aprimorarHabilidadePrincipal(){

    }
    public void regenerar(){

    }
}
