package com.company;

import com.util.Util;

import java.util.Random;

public abstract class Warrior extends Personagem{

    public Warrior(String nome){
        this.nome = String.format("%s Warrior", nome);
        this.forca = Util.random(1,1000);
        this.habilidadeMental = 0;
        this.poderDeAtaque = forca * 10;
        this.esquiva = Util.random(0,50);
        this.resistencia = Util.random(0,90);
        this.vidaMaxima = Util.random(1,40000);
        this.vidaAtual = vidaMaxima;
    }

    @Override
    public void atacar(Personagem oponente){
        //Considerar esquiva
        // poder de ataque - resistência
        // contraataque
        boolean dodge = (Util.random(0, 100)<oponente.esquiva);
        if(!dodge){
            oponente.vidaAtual =
                    oponente.vidaAtual - (poderDeAtaque -
                            (poderDeAtaque * (oponente.resistencia/100)));

        }
        contraatacar(this);
    }


    @Override
    public void contraatacar(Personagem oponente){
        boolean dodge = (Util.random(0, 100)<oponente.esquiva);
        if(!dodge){
            oponente.vidaAtual =
                    oponente.vidaAtual - (poderDeAtaque -
                            (poderDeAtaque * (oponente.resistencia/100)));

        }
    }

    @Override
    public void aprimorarHabilidadePrincipal(){
        forca = (int)(forca * 1.1);
    }
    @Override
    public void regenerar(){
        int novaVida = (vidaAtual+(int)(vidaMaxima*0.05));
        if(novaVida > vidaMaxima){
            vidaAtual = vidaMaxima;
        }else{
            vidaAtual = novaVida;
        }
    }


}
